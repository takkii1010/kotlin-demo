package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("user")
class UserController {

    @GetMapping
    fun index(@RequestParam(name = "inputName") inputName: String, model: Model): String {
        model.addAttribute("yourName", inputName)
        return "index"
    }
}