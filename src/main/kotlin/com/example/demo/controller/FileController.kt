package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("file")
class FileController {

    @GetMapping
    fun showInput(model: Model): String {
        model.addAttribute(InputContentForm())
        return "file/input"
    }

    @PostMapping("output")
    fun outputTextFileFormat(inputContentForm: InputContentForm) {
        println(inputContentForm)
    }
}

data class InputContentForm(
        val email: String? = null,
        val password: String? = null,
        val address: String? = null,
        val city: String? = null,
        val state: String? = null,
        val zip: String? = null,
        val fileFormatOptions: String? = null
)